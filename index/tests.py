from django.test import TestCase,LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import postingan
from .forms import postForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

class Lab_6_Test(TestCase):

	def test_tdd_url_exists(self):
		response = Client().get('/tdd/')
		self.assertEqual(response.status_code, 200)

	def test_lab5_using_index_func(self):
		found = resolve('/tdd/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_post(self):
		# Creating a new activity
		new_post = postingan.objects.create(content = 'sedih')

		# Retrieving all available activ
		counting_all_available_post = postingan.objects.all().count()
		self.assertEqual(counting_all_available_post, 1)

class PyhtonLab6Test(LiveServerTestCase):
	def setUp(self):
		self.driver = webdriver.Chrome()
		super(PyhtonLab6Test,self).setUp()

	def tearDown(self):
		self.driver.implicitly_wait(3)
		self.driver.quit()
		super(PyhtonLab6Test,self).tearDown()

	def test_cobacoba(self):
		self.driver.get('http://localhost:8000/tdd/')
		time.sleep(5)
		search_box = self.driver.find_element_by_name('content')
		search_box.send_keys('Coba coba')
		search_box.submit()
		time.sleep(5)
		self.assertIn("Coba coba", self.driver.page_source)

	def test_h1_with_css_property(self):
		self.driver.get('http://localhost:8000/tdd/')
		h1_text = self.driver.find_element_by_name('h1').value_of_css_property('text-align')
		time.sleep(5)
		self.assertIn('center', h1_text)

	def test_h1_second_css_property(self):
		self.driver.get('http://localhost:8000/tdd/')
		h1_text = self.driver.find_element_by_name('h1').value_of_css_property('font-weight')
		time.sleep(5)
		self.assertIn('700', h1_text)

	def test_h1_with_css_property(self):
		self.driver.get('http://localhost:8000/tdd/')
		h1_text = self.driver.find_element_by_name('see').value_of_css_property('text-align')
		time.sleep(5)
		self.assertIn('center', h1_text)

	def test_h1_second_css_property(self):
		self.driver.get('http://localhost:8000/tdd/')
		h1_text = self.driver.find_element_by_name('see').value_of_css_property('font-weight')
		time.sleep(5)
		self.assertIn('700', h1_text)