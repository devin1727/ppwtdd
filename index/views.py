from django.shortcuts import render, redirect
from .forms import postForm
from .models import postingan

def index(request):
	form = postForm(request.POST or None)
	response = {}
	if request.method == 'POST':
		if form.is_valid():
			data = postingan(content = request.POST.get('content'))
			data.save()
			return redirect('index')
	else :
		data = postingan.objects.all()
		response = {
			"data" : data,
			"form" : form
		}
		return render(request,'index.html',response)

def profile(request):
	return render(request, 'profile.html')
		